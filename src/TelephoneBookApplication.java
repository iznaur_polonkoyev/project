import java.util.Scanner;

public class TelephoneBookApplication {

    public static void main(String[] args) {

        System.out.println("Hello this is my phonebook");

        Scanner scanner = new Scanner(System.in);

        TelBook telBook = new TelBook();

        while (true) {

            System.out.println("Menu: ");
            System.out.println("1 - Add page;");
            System.out.println("2 - Delete page;");
            System.out.println("3 - Show all pages;");

            System.out.println("Enter option: ");
            int optionSelected = Integer.parseInt(scanner.nextLine());

            if (optionSelected == 1) {

                Birthday birthday = new Birthday();

                System.out.println("Birthday:");
                System.out.println("day:");
                birthday.setDay(Integer.parseInt(scanner.nextLine()));
                System.out.println("month:");
                birthday.setMonth(Integer.parseInt(scanner.nextLine()));
                System.out.println("year:");
                birthday.setYear(Integer.parseInt(scanner.nextLine()));

                Name name = new Name();

                System.out.println("Name:");
                System.out.println("firstname:");
                name.setFirstname(scanner.nextLine());
                System.out.println("surname:");
                name.setSurname(scanner.nextLine());
                System.out.println("paternalname:");
                name.setPaternal(scanner.nextLine());
                System.out.println("maternalname:");
                name.setMaternal(scanner.nextLine());
                System.out.println("prefix:");
                name.setPrefix(scanner.nextLine());

                MobilePhone mobilePhone = new MobilePhone();

                System.out.println("Mobile phone:");
                System.out.println("country code:");
                mobilePhone.setCountryCode(scanner.nextLine());
                System.out.println("msisdn:");
                mobilePhone.setMsisdn(scanner.nextLine());

                Gender gender = new Gender();

                System.out.println("Type");
                gender.setType(scanner.nextLine());

                Profession profession = new Profession();

                System.out.println("Prifession group");
                profession.setProfessionGroup(scanner.nextLine());
                System.out.println("ProfessionName");
                profession.setProfessionName(scanner.nextLine());

                Email email = new Email();

                System.out.println("username");
                email.setUsername(scanner.nextLine());
                System.out.println("mailProvider");
                email.setMailProvider(scanner.nextLine());

                FacebookAccount facebookAccount = new FacebookAccount();

                System.out.println("username");
                facebookAccount.setUsername(scanner.nextLine());

                Person person = new Person();

                person.setName(name);
                person.setBirthday(birthday);
                person.setMobilePhone(mobilePhone);

                person.setGender(gender);
                person.setProfession(profession);
                person.setEmail(email);
                person.setFacebookAccount(facebookAccount);

                Page page = new Page();

                page.setPerson(person);

                telBook.add(page);

            } else if (optionSelected == 3) {

                int i = 0;

                Page[] pages = telBook.getPages();

                while (i < telBook.getLastEmptyPageIndex()) {

                    System.out.print(pages[i].getPerson().getName().getFirstname() + " \n");
                    System.out.print(pages[i].getPerson().getName().getSurname() + " \n");
                    System.out.print(pages[i].getPerson().getName().getPaternal() + " \n");
                    System.out.print(pages[i].getPerson().getName().getMaternal() + " \n");
                    System.out.print(pages[i].getPerson().getName().getPrefix() + " \n");
                    System.out.print(pages[i].getPerson().getMobilePhone().getMsisdn() + " \n");
                    System.out.print(pages[i].getPerson().getMobilePhone().getCountryCode() + " \n");
                    System.out.print(pages[i].getPerson().getBirthday().getDay() + " \n");
                    System.out.print(pages[i].getPerson().getBirthday().getMonth() + " \n");
                    System.out.print(pages[i].getPerson().getBirthday().getYear() + " \n");
                    System.out.print(pages[i].getPerson().getGender().getType() + " \n");
                    System.out.print(pages[i].getPerson().getProfession().getProfessionName() + " \n");
                    System.out.print(pages[i].getPerson().getProfession().getProfessionGroup() + " \n");
                    System.out.print(pages[i].getPerson().getEmail().getUsername() + " \n");
                    System.out.print(pages[i].getPerson().getEmail().getMailProvider() + " \n");
                    System.out.print(pages[i].getPerson().getFacebookAccount().getUsername() + " \n");

                    i++;
                }

            }
        }

    }

}
