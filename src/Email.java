public class Email {

    private String username; //maxim.semenov
    private String mailProvider; //gmail.com

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMailProvider() {
        return mailProvider;
    }

    public void setMailProvider(String mailProvider) {
        this.mailProvider = mailProvider;
    }
}
