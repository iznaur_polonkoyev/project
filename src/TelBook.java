public class TelBook {

    private String owner;
    private Page[] pages = new Page[10];
    private int lastEmptyPageIndex = 0;

    public void add(Page page) {
        pages[lastEmptyPageIndex] = page;
        lastEmptyPageIndex++;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Page[] getPages() {
        return pages;
    }

    public void setPages(Page[] pages) {
        this.pages = pages;
    }

    public int getLastEmptyPageIndex() {
        return lastEmptyPageIndex;
    }

    public void setLastEmptyPageIndex(int lastEmptyPageIndex) {
        this.lastEmptyPageIndex = lastEmptyPageIndex;
    }
}
