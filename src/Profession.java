public class Profession {

    private String professionGroup; //medical
    private String professionName; //dentist

    public String getProfessionGroup() {
        return professionGroup;
    }

    public void setProfessionGroup(String professionGroup) {
        this.professionGroup = professionGroup;
    }

    public String getProfessionName() {
        return professionName;
    }

    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }
}
